@extends('adminlte.master')

@section('content')
<div class="card m-3">
    <div class="card-header">
      <h3 class="card-title">Cast List</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <a class="btn btn-primary mb-2" href="/cast/create">Add New Cast</a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">No</th>
            <th>Name</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($list as $key => $item)
              <tr>
                  <td>{{  $key + 1 }}</td>
                  <td>{{  $item->name }}</td>
                  <td>{{  $item->umur }}</td>
                  <td>{{  $item->bio }}</td>
                  <td style="display: flex;">
                    <a class="btn btn-primary btn-sm ml-2" href="/cast/{{ $item->id }}">Detail</a>
                    <a class="btn btn-primary btn-sm ml-2" href="/cast/{{ $item->id }}/edit">Edit</a>
                    <form action="/cast/{{ $item->id }}" method="post">
                    @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2">
                    </form>
                  </td>
              </tr>

              @empty
            <tr>
                <td colspan="4" align="center"> No Data</td>
            </tr>
              
          @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->

  </div>
   
@endsection
