@extends('adminlte.master')

@section('content')
<div class="card card-primary m-3">
    <div class="card-header">
      <h3 class="card-title">Edit Cast {{ old('id', $cast->id) }}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/cast/{{$cast->id}}" method="POST">
      @csrf
      @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="name">Name :</label>
          <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $cast->name) }}" placeholder="Enter name">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="umur">Umur :</label>
          <input type="text" class="form-control" name="umur" id="umur" value="{{ old('umur', $cast->umur) }}"placeholder="Enter age">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="bio">Bio :</label>
          <textarea class="form-control" rows="5" name="bio" id="bio" placeholder="Bio">{{ old('bio', $cast->bio) }}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary" value="submit">Submit</button>
      </div>
    </form>
  </div>
   
@endsection
