<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CastController extends Controller
{
    //---------Index-----------------
    public function index(){
        $list = DB::table('cast')->get();
        return view('post/index', compact('list'));
    }
    //---------Create-----------------
    public function create(){
        return view('post/create');
    }
    //---------Store-----------------
    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table("cast")->insert([
            "name" => $request['name'],
            "umur" => $request['umur'],
            "bio" => $request['bio']
        ]);
        return redirect('cast')->with('success', 'Data Berhasil Ditambahkan!');
    }
    //---------Show-----------------
    public function show($cast_id){
        $cast = DB::table('cast')->where('id',$cast_id)->first();
        
        return view('post/show',compact('cast'));
    }
    //---------Edit-----------------
    public function edit($cast_id){
        $cast = DB::table('cast')->where('id',$cast_id)->first();
        return view('post/edit',compact('cast'));
    }
    //---------Update-----------------
    public function update($cast_id, Request $request){
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $update = DB::table('cast')
        ->where('id',$cast_id)
        ->update([
            "name" => $request['name'],
            "umur" => $request['umur'],
            "bio" => $request['bio']
        ]);
        return redirect('cast')->with('success', 'Data Berhasil Diubah!');
    }
    //---------Destroy-----------------
    public function destroy($cast_id){
        $delete = DB::table('cast')
        ->where('id',$cast_id)
        ->delete();
        return redirect('cast')->with('success', 'Data Berhasil Dihapus!');
    }
}
